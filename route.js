module.exports = (app) => {
    const person = require('./controller/person-controller.js');

    
    app.post('/person', person.create);

   
    app.get('/person', person.findAll);

  
    app.get('/person/:personId', person.findOne);


    app.put('/person/:personId', person.update);

    // Delete a Note with noteId
    app.delete('/person/:personId', person.delete);
}