const Person = require('../model/person-model.js');

exports.create = (req, res) => {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');
    // Create a Note
    const person = new Person({
        firstname: req.body.firstname, 
        lastname: req.body.lastname
    });

    // Save Note in the database
    
    person.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Person."
        });
    });
};
exports.findAll = (req, res) => {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');
    Person.find()
    .then(person => {
        res.send(person);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving person."
        });
    });
};

exports.findOne = (req, res) => {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');
    Person.findById(req.params.personId)
    .then(person => {
        if(!person) {
            return res.status(404).send({
                message: "Person not found with id " + req.params.personId
            });            
        }
        res.send(person);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Person not found with id " + req.params.personId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving person with id " + req.params.personId
        });
    });
};

exports.update = (req, res) => {

    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');
    Person.findByIdAndUpdate(req.params.personId, {
        firstname: req.body.firstname,
        lastname: req.body.lastname
    }, {new: true})
    .then(person => {
        if(!person) {
            return res.status(404).send({
                message: "person not found with id " + req.params.personId
            });
        }
        res.send(person);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.personId
            });                
        }
        return res.status(500).send({
            message: "Error updating person with id " + req.params.personId
        });
    });
};

exports.delete = (req, res) => {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');
    Person.findByIdAndRemove(req.params.personId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Person not found with id " + req.params.personId
            });
        }
        res.send({message: "Person deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "person not found with id " + req.params.personId
            });                
        }
        return res.status(500).send({
            message: "Could not delete person with id " + req.params.personId
        });
    });
};