const mongoose = require('mongoose');

const Person = mongoose.Schema({
    firstname: String,
    lastname: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Person', Person);