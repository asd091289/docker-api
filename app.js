const express = require('express');
const morgan = require('morgan'); //for logging
const bodyParser = require('body-parser');

//resources
const message = require('./resource/message');
const person = require('./resource/person');
const dbConfig = require('./config/config.js');
const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const options = {
    useNewUrlParser : true,
    useCreateIndex  : true,
    autoReconnect: true,
    replicaSet: 'rs0',
    connectWithNoPrimary: true,
};


// Connecting to the database
mongoose.connect(dbConfig.url,options).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

const app = express();

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());



app.use((req,res,next)=>{ //allow cros
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods','PUT, GET, POST, DELETE');
        return res.status(200).json({});
    }
    next();
})

//endpoints

require('./route.js')(app);
app.use('/message',message);

app.use('/person1',person);

//error handling 
app.use((req,res,next)=>{
    const error = new Error('Error 404 not found');
    error.status =404;
    next(error);
})
app.use((error,req,res,next)=>{
    res.status(error.status || 500)
        .json({
            error:{
                message:error.message
            }
        });
    
})

module.exports = app;