const express = require('express');
const router = express.Router();

router.get('/',(req,res,next)=>{
    res.status(200).json({
        message:"it works! in get"
    })
})

router.get('/:id',(req,res,next)=>{
    const id = req.params.id;
    res.status(200).json({
        message:"find by Id no "+ id,
        id:id
    })
})

router.post('/',(req,res,next)=>{
    res.status(201).json({
        message:"it works! in post"
    })
})

module.exports = router;