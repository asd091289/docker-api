FROM node:10.16.3-alpine

WORKDIR /usr/app
COPY package.json /usr/app

RUN npm install
COPY . /usr/app

EXPOSE 3000

 CMD [ "node", "server.js" ]